<%--
    Document   : index
    Created on : 15 juin 2017, 19:59:18
    Author     : Patrick DAPA
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- css files declaration -->
        <link rel="stylesheet" type="text/css" href="css/app.css" />
        <link rel="stylesheet" type="text/css" href="css/datatables.css" />
        <link rel="stylesheet" type="text/css" href="css/semantic.css" />

        <!-- favicon definition -->
        <link rel="icon" type="image/ico" href="../../img/favicon/ldnr-icon-72x72.png" />

        <!-- general style definition -->
        <style type="text/css">

            .hidden.menu {
                display: none;
            }

            .masthead.segment {
                min-height: 80px;
                padding: 1em 0em;
            }
            .masthead .logo.item img {
                margin-right: 1em;
            }
            .masthead .ui.menu .ui.button {
                margin-left: 0.5em;
            }
            .masthead h1.ui.header {
                margin-top: 3em;
                margin-bottom: 0em;
                font-size: 4em;
                font-weight: normal;
            }
            .masthead h2 {
                font-size: 1.7em;
                font-weight: normal;
            }

            .ui.vertical.stripe {
                padding: 8em 0em;
            }
            .ui.vertical.stripe h3 {
                font-size: 2em;
            }
            .ui.vertical.stripe .button + h3,
            .ui.vertical.stripe p + h3 {
                margin-top: 3em;
            }
            .ui.vertical.stripe .floated.image {
                clear: both;
            }
            .ui.vertical.stripe p {
                font-size: 1.33em;
            }
            .ui.vertical.stripe .horizontal.divider {
                margin: 3em 0em;
            }

            .quote.stripe.segment {
                padding: 0em;
            }
            .quote.stripe.segment .grid .column {
                padding-top: 5em;
                padding-bottom: 5em;
            }

            .footer.segment {
                padding: 5em 0em;
            }

            .secondary.pointing.menu .toc.item {
                display: none;
            }

            @media only screen and (max-width: 700px) {
                .ui.fixed.menu {
                    display: none !important;
                }
                .secondary.pointing.menu .item,
                .secondary.pointing.menu .menu {
                    display: none;
                }
                .secondary.pointing.menu .toc.item {
                    display: block;
                }
                .masthead.segment {
                    min-height: 350px;
                }
                .masthead h1.ui.header {
                    font-size: 2em;
                    margin-top: 1.5em;
                }
                .masthead h2 {
                    margin-top: 0.5em;
                    font-size: 1.5em;
                }
            }
        </style>
        <title>LDNR | La Distance Nous Rapproche</title>
    </head>
    <body>
        <header>
            <!-- Contenu de la page -->
            <div class="pusher">
                <div class="ui inverted vertical masthead center aligned segment">
                    <div class="ui container">
                        <div class="ui large secondary inverted pointing menu">
                            <div class="item">
                                <a href='/Bd'>ACCUEIL</a>
                            </div>
                            <form id="listAllAlbums" action="AlbumsManagement" method="post">
                                <div class="item">
                                    <a href="#" onclick="javascript:document.getElementById('listAllAlbums').submit();">ALBUMS DE LA BASE</a>
                                </div>
                            </form>
                            <form id="listAllSamples" action="SamplesManagement" method="post">
                                <div class="item">
                                    <a href="#" onclick="javascript:document.getElementById('listAllSamples').submit();">EXEMPLAIRES DE LA BASE</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="pusher">
            <div class="ui container">
                <div class="content">
                    <table id="albums" class="ui celled table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Titre</th>
                                <th>Première Edition?</th>
                                <th>Editer</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Titre</th>
                                <th>Première Edition?</th>
                                <th>Editer</th>
                                <th>Supprimer</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${requestScope.samples}" var="sample">
                                <tr>
                                    <td>${sample.year}</td>
                                    <td>${sample.firstEdition}</td>
                                    <td><i onclick='javascript:open_modal(edit${sample.id})' title="modifier" class="icon edit"></i></td>
                                    <td><i onclick='javascript:open_modal(del${sample.id})' title="supprimer" class="icon erase"></i></td>
                                        <jsp:element name="div">
                                            <jsp:attribute name="id">
                                            edit${sample.id}
                                        </jsp:attribute>
                                        <jsp:attribute name="class">
                                            ui modal
                                        </jsp:attribute>
                                        <jsp:body>
                                            <jsp:element name="form">
                                                <jsp:attribute name="action">SamplesManagement</jsp:attribute>
                                                <jsp:attribute name="method">post</jsp:attribute>
                                                <jsp:attribute name="id">edition_form${sample.id}</jsp:attribute>
                                                <jsp:body>
                                            <div class="header">Modification de l'exemplaire Numéro : ${sample.id}</div>
                                            <div class='content'>
                                                <div class="ui input">
                                                    <input name='year' type="number" value='${sample.year}' />
                                                </div>
                                                <div class="ui input">
                                                    <select name='firstEdition'>
                                                        <option value='true'>oui</option>
                                                        <option value="false">non</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="actions">
                                                <div onclick="javascript:document.getElementById('edition_form${sample.id}').submit();" class="ui positive right labeled icon button">
                                                    <span>Valider</span>
                                                </div>
                                                <div class="ui black deny button">
                                                    Annuler
                                                </div>
                                            </div>
                                            <input type='hidden' name="action" value="edit" />
                                            <input name="idSample" type="hidden" value="${sample.id}">
                                        </jsp:body>
                                    </jsp:element>
                                </jsp:body>
                            </jsp:element>
                            <jsp:element name="div">
                                <jsp:attribute name="id">
                                    del${sample.id}
                                </jsp:attribute>
                                <jsp:attribute name="class">
                                    ui modal
                                </jsp:attribute>
                                <jsp:body>
                                    <jsp:element name="form">
                                        <jsp:attribute name="action">SamplesManagement</jsp:attribute>
                                        <jsp:attribute name="method">post</jsp:attribute>
                                        <jsp:attribute name="id">deletion_form${sample.id}</jsp:attribute>
                                        <jsp:body>
                                            <div class="header">Suppression de l'Exemplaire Numéro : ${sample.id}</div>
                                            <div class="actions">
                                                <div class="ui positive right labeled icon button">
                                                    <span onclick="javascript:document.getElementById('deletion_form${sample.id}').submit();">Valider</span>
                                                </div>
                                                <div class="ui black deny button">
                                                    Annuler
                                                </div>
                                            </div>
                                            <input name="action" type="hidden" value='delete' />
                                            <input name="idSample" type="hidden" value="${sample.id}">
                                        </jsp:body>
                                    </jsp:element>
                                </jsp:body>
                            </jsp:element>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="ui modal" id="create">
                        <form id='insertion_form' action='SamplesManagement' method="post">
                            <div class="header">Création d'un Exemplaire</div>
                            <div class="content">
                                <div class="ui input">
                                    <input type='number' name='year' required />
                                </div>
                                <div class="ui input">
                                    <select name='firstEdition'>
                                        <option value='true'>oui</option>
                                        <option value="false">non</option>
                                    </select>
                                </div>
                            </div>
                            <div class="actions">
                                <div onclick="javascript:document.getElementById('insertion_form').submit();" class="ui positive right labeled icon button">
                                    <span>Valider</span>
                                </div>
                                <div class="ui black deny button">
                                    <span>Annuler</span>
                                </div>
                            </div>
                            <input type='hidden' name="action" value="create" />
                        </form>
                    </div>
                    <div onclick="javascript:open_modal('#create');" class ="ui positive right labeled icon button">
                        <span>Création d'un exemplaire</span>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="ui inverted vertical footer segment">
                <div class="ui container">
                    <div class="ui stackable inverted divided equal height stackable grid">
                        <div class="three wide column">
                            <h4 class="ui inverted header">A propos</h4>
                            <div class="ui inverted link list">
                                <a href="#" class="item">Mentions Légales</a>
                                <a href="#" class="item">Nous Contacter</a>
                            </div>
                        </div>
                        <div class="six wide column">
                            <h3>"N'hésitez pas à nous appeler au"</h3>
                            <p>05 61 00 14 85</p>
                            <img src='img/mascotte.png' />
                        </div>
                        <div class="five wide column">
                            <h3>"GéoLocalisation"</h3>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2891.672896923598!2d1.5012029999999998!3d43.55086000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebe7555fe38bb%3A0x88589432d0a7884d!2sLDNR!5e0!3m2!1sfr!2sfr!4v1421674126713" width="400" height="300" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="js/jquery.js"></script>
        <script src="js/semantic.js"></script>
        <script src='js/app.js'></script>
        <script src="js/dataTable.min.js"></script>
        <script src="js/dataTable.semantic.js"></script>
    </body>
</html>
