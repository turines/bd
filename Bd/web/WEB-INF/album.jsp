<%--
    Document   : index
    Created on : 15 juin 2017, 19:59:18
    Author     : Patrick DAPA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- css files declaration -->
        <link rel="stylesheet" type="text/css" href="css/app.css" />
        <link rel="stylesheet" type="text/css" href="css/datatables.css" />
        <link rel="stylesheet" type="text/css" href="css/semantic.css" />

        <!-- favicon definition -->
        <link rel="icon" type="image/ico" href="../../img/favicon/ldnr-icon-72x72.png" />

        <!-- general style definition -->
        <style type="text/css">

            .hidden.menu {
                display: none;
            }

            .masthead.segment {
                min-height: 80px;
                padding: 1em 0em;
            }
            .masthead .logo.item img {
                margin-right: 1em;
            }
            .masthead .ui.menu .ui.button {
                margin-left: 0.5em;
            }
            .masthead h1.ui.header {
                margin-top: 3em;
                margin-bottom: 0em;
                font-size: 4em;
                font-weight: normal;
            }
            .masthead h2 {
                font-size: 1.7em;
                font-weight: normal;
            }

            .ui.vertical.stripe {
                padding: 8em 0em;
            }
            .ui.vertical.stripe h3 {
                font-size: 2em;
            }
            .ui.vertical.stripe .button + h3,
            .ui.vertical.stripe p + h3 {
                margin-top: 3em;
            }
            .ui.vertical.stripe .floated.image {
                clear: both;
            }
            .ui.vertical.stripe p {
                font-size: 1.33em;
            }
            .ui.vertical.stripe .horizontal.divider {
                margin: 3em 0em;
            }

            .quote.stripe.segment {
                padding: 0em;
            }
            .quote.stripe.segment .grid .column {
                padding-top: 5em;
                padding-bottom: 5em;
            }

            .footer.segment {
                padding: 5em 0em;
            }

            .secondary.pointing.menu .toc.item {
                display: none;
            }

            @media only screen and (max-width: 700px) {
                .ui.fixed.menu {
                    display: none !important;
                }
                .secondary.pointing.menu .item,
                .secondary.pointing.menu .menu {
                    display: none;
                }
                .secondary.pointing.menu .toc.item {
                    display: block;
                }
                .masthead.segment {
                    min-height: 350px;
                }
                .masthead h1.ui.header {
                    font-size: 2em;
                    margin-top: 1.5em;
                }
                .masthead h2 {
                    margin-top: 0.5em;
                    font-size: 1.5em;
                }
            }
        </style>
        <title>LDNR | La Distance Nous Rapproche</title>
    </head>
    <body>
        <header>
            <!-- Contenu de la page -->
            <div class="pusher">
                <div class="ui inverted vertical masthead center aligned segment">
                    <div class="ui container">
                        <div class="ui large secondary inverted pointing menu">
                            <div class="item">
                                <a href='index.jsp'>ACCUEIL</a>
                            </div>
                            <div class="item">
                                <a href="albums.jsp">ALBUMS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="pusher">
            <div class="ui container">
                <div class="content">
                </div>
            </div>
        </div>
        <footer>
            <div class="ui inverted vertical footer segment">
                <div class="ui container">
                    <div class="ui stackable inverted divided equal height stackable grid">
                        <div class="three wide column">
                            <h4 class="ui inverted header">A propos</h4>
                            <div class="ui inverted link list">
                                <a href="#" class="item">Mentions Légales</a>
                                <a href="#" class="item">Nous Contacter</a>
                            </div>
                        </div>
                        <div class="six wide column">
                            <h3>"N'hésitez pas à nous appeler au"</h3>
                            <p>05 61 00 14 85</p>
                            <img src='img/mascotte.png' />
                        </div>
                        <div class="five wide column">
                            <h3>"GéoLocalisation"</h3>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2891.672896923598!2d1.5012029999999998!3d43.55086000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebe7555fe38bb%3A0x88589432d0a7884d!2sLDNR!5e0!3m2!1sfr!2sfr!4v1421674126713" width="400" height="300" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="js/jquery.js"></script>
        <script src="js/semantic.js"></script>
        <script src='js/app.js'></script>
        <script src="js/dataTable.min.js"></script>
        <script src="js/dataTable.semantic.js"></script>
    </body>
</html>
