<%--
    Document   : index
    Created on : 15 juin 2017, 20:11:54
    Author     : Patrick DAPA
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- css files declaration -->
        <link rel="stylesheet" type="text/css" href="css/semantic.css" />
        <link rel="stylesheet" type="text/css" href="css/app.css" />
        <link rel='stylesheet' type='text/css' href='css/dropdown.css'/>
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <!-- icons files declaration -->
        <link rel="icon" href="icon/icons.ttf" />
        <link rel="icon" href="icon/icons.woff" />
        <link rel="icon" href="icon/icons.woff2" />

        <!-- favicon definition -->
        <link rel="icon" type="image/ico" href="img/favicon/ldnr-icon-72x72.png" />

        <!-- general style definition -->
        <style type="text/css">

            .hidden.menu {
                display: none;
            }

            .masthead.segment {
                min-height: 700px;
                padding: 1em 0em;
            }
            .masthead .logo.item img {
                margin-right: 1em;
            }
            .masthead .ui.menu .ui.button {
                margin-left: 0.5em;
            }
            .masthead h1.ui.header {
                margin-top: 3em;
                margin-bottom: 0em;
                font-size: 4em;
                font-weight: normal;
            }
            .masthead h2 {
                font-size: 1.7em;
                font-weight: normal;
            }

            .ui.vertical.stripe {
                padding: 8em 0em;
            }
            .ui.vertical.stripe h3 {
                font-size: 2em;
            }
            .ui.vertical.stripe .button + h3,
            .ui.vertical.stripe p + h3 {
                margin-top: 3em;
            }
            .ui.vertical.stripe .floated.image {
                clear: both;
            }
            .ui.vertical.stripe p {
                font-size: 1.33em;
            }
            .ui.vertical.stripe .horizontal.divider {
                margin: 3em 0em;
            }

            .quote.stripe.segment {
                padding: 0em;
            }
            .quote.stripe.segment .grid .column {
                padding-top: 5em;
                padding-bottom: 5em;
            }

            .footer.segment {
                padding: 5em 0em;
            }

            .secondary.pointing.menu .toc.item {
                display: none;
            }

            @media only screen and (max-width: 700px) {
                .ui.fixed.menu {
                    display: none !important;
                }
                .secondary.pointing.menu .item,
                .secondary.pointing.menu .menu {
                    display: none;
                }
                .secondary.pointing.menu .toc.item {
                    display: block;
                }
                .masthead.segment {
                    min-height: 350px;
                }
                .masthead h1.ui.header {
                    font-size: 2em;
                    margin-top: 1.5em;
                }
                .masthead h2 {
                    margin-top: 0.5em;
                    font-size: 1.5em;
                }
            }
        </style>
        <title>LDNR | Gestion Des Albums</title>
    </head>
    <body>

        <!-- Contenu de la page -->
        <div class="pusher">
            <div class="ui inverted vertical masthead center aligned segment">
                <div class="ui text container">
                    <div class="ui slider">
                        <h1 class="ui inverted header">
                            BD - THEQUE LDNR
                        </h1>
                        <h2>La Distance Nous Rapproche</h2>
                        <div>
                            <form id="albums" action="AlbumsManagement" method="post">
                                <a class="ui huge primary button" onclick="javascript:document.getElementById('albums').submit();">Gestion des Albums!</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui vertical stripe segment">
                <div class="ui middle aligned stackable grid container">
                    <div class="row">
                        <div class="fourteen wide column paragraphe">
                            <h3 class="ui header">LDNR</h3>
                            <h2>La Distance Nous Rapproche</h2>
                            <p>LDNR est un organisme de formation qui propose des formations informatique en salle ou à distance.
                                Nos formations couvrent tous les domaines autour du projet informatique: de l’apprentissage des techniques
                                (langages de programmation, système d’exploitation, bureautique…), à la gestion de projet informatique en
                                passant par des aspects management et recrutement.</p>
                            <h3 class="ui header">VOS FORMATEURS</h3>
                            <h2>Vos Experts</h2>
                            <p>Nos experts
                                Notre société est composée d’experts en informatique présents sur le secteur de la formation depuis de nombreuses
                                années. Nos formateurs ont exercé tant en prestation de service informatique qu’en formation.</p>
                            <h3 class="ui header">PÉDAGOGIE</h3>
                            <h2>Compétences & pédagogie</h2>
                            <p>Nos formateurs disposent de compétences techniques indéniables et sont reconnus pour leur savoir-faire pédagogique
                                et leur engouement à transmettre les connaissances.</p>
                            <h3 class="ui header">NOS EQUIPES</h3>
                            <h2>Des Experts</h2>
                            <p>Notre équipe technique installe et valide les salles de formation, mais effectue également une veille active
                                sur les nouveaux outils pédagogiques qu’elle met à disposition des formateurs.
                                Notre équipe pédago-administrative vous accompagne à la mise en place de votre plan de formation et assure la liaison avec vos services.</p>
                            <h3 class="ui header">NOS EQUIPES</h3>
                            <h2>NOTRE FORCE</h2>
                            <p>Réactivité et personnalisation
                                Notre organisme étant à taille humaine, nous avons une réactivité qui saura vous satisfaire. Un interlocuteur unique
                                vous sera dédié dès confirmation de votre projet.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui inverted vertical footer segment">
                <div class="ui container">
                    <div class="ui stackable inverted divided equal height stackable grid">
                        <div class="three wide column">
                            <h4 class="ui inverted header">A propos</h4>
                            <div class="ui inverted link list">
                                <a href="#" class="item">Mentions Légales</a>
                                <a href="#" class="item">Nous Contacter</a>
                            </div>
                        </div>
                        <div class="six wide column">
                            <h3>"N'hésitez pas à nous appeler au"</h3>
                            <p>05 61 00 14 85</p>
                            <img src='img/mascotte.png' />
                        </div>
                        <div class="five wide column">
                            <h3>"GéoLocalisation"</h3>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2891.672896923598!2d1.5012029999999998!3d43.55086000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebe7555fe38bb%3A0x88589432d0a7884d!2sLDNR!5e0!3m2!1sfr!2sfr!4v1421674126713" width="400" height="300" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/semantic.js"></script>
        <script src='js/app.js'></script>
        <script src='js/index/index.js'></script>
        <script src="js/dataTable.semantic.js"></script>
        <script>
                                    $(document)
                                            .ready(function () {

                                                // fix menu when passed
                                                $('.masthead')
                                                        .visibility({
                                                            once: false,
                                                            onBottomPassed: function () {
                                                                $('.fixed.menu').transition('fade in');
                                                            },
                                                            onBottomPassedReverse: function () {
                                                                $('.fixed.menu').transition('fade out');
                                                            }
                                                        })
                                                        ;

                                                // create sidebar and attach to menu open
                                                $('.ui.sidebar')
                                                        .sidebar('attach events', '.toc.item')
                                                        ;

                                            })
                                            ;
        </script>
    </body>
</html>

