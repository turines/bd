/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.model;

import com.albums.dao.AlbumDAO;
import com.albums.dao.DAOFactory;
import com.albums.orm.Album;
import com.albums.orm.ORMFactory;
import java.util.List;

/**
 *
 * @author Patrick DAPA
 */
public class AlbumManager {

    public AlbumManager() {

    }

    public void addAlbumToCollection(String pTitle, String pSerie) {
        Album album = ORMFactory.getAlbum(pTitle, pSerie);
        AlbumDAO albumDAO = DAOFactory.getAlbumDAO();
        albumDAO.create(album);
    }

    public void RemoveAlbumFromCollection(long id) {
        AlbumDAO dAO = DAOFactory.getAlbumDAO();
        dAO.delete(id);
    }

    public List<Album> listAllAlbums() {
        AlbumDAO dAO = DAOFactory.getAlbumDAO();
        List<Album> albums = dAO.findAll();
        return albums;
    }

    public void updateAlbum(Album album) {
        AlbumDAO dAO = DAOFactory.getAlbumDAO();
        dAO.update(album);
    }
}
