/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.model;

/**
 *
 * @author Patrick DAPA
 */
public class ManagerFactory {

    public static AlbumManager getAlbumManager() {
        return new AlbumManager();
    }

    public static SampleManager getSampleManager() {
        return new SampleManager();
    }
}
