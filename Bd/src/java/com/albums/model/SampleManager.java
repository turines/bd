/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.model;

import com.albums.dao.AlbumDAO;
import com.albums.dao.DAOFactory;
import com.albums.dao.SampleDAO;
import com.albums.orm.Album;
import com.albums.orm.ORMFactory;
import com.albums.orm.Sample;
import java.util.List;

/**
 *
 * @author Patrick DAPA
 */
public class SampleManager {

    public SampleManager() {

    }

    public void addSampleToCollection(int pYear, boolean pIsFirstEdition) {
        Sample sample = ORMFactory.getSample(pYear, pIsFirstEdition);
        SampleDAO sampleDAO = DAOFactory.getSampleDAO();
        sampleDAO.create(sample);
    }

    public void addSampleToAlbum(int pYear, boolean pIsFirstEdition, long idAlbum) {
        Sample sample = ORMFactory.getSample(pYear, pIsFirstEdition);
        AlbumDAO albumDAO = DAOFactory.getAlbumDAO();
        Album album = albumDAO.findById(idAlbum);
        sample.setAlbum(album);
        SampleDAO sampleDAO = DAOFactory.getSampleDAO();
        sampleDAO.create(sample);
    }

    public void RemoveSampleFromAlbum(long idSample) {
        SampleDAO dAO = DAOFactory.getSampleDAO();
        dAO.delete(idSample);
    }

    public List<Sample> listAllSamples() {
        SampleDAO dAO = DAOFactory.getSampleDAO();
        List<Sample> samples = dAO.findAll();
        return samples;
    }

    public void updateSample(Sample sample) {
        SampleDAO dAO = DAOFactory.getSampleDAO();
        dAO.update(sample);
    }

    public List<Sample> listSamplesByAlbum(long idAlbum) {
        SampleDAO dAO = DAOFactory.getSampleDAO();
        return dAO.findByAlbumId(idAlbum);
    }
}
