/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.service;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author stag
 */
public class EMF {

    private static EntityManagerFactory emf;

    private static final String PERSISTENCE_UNIT_NAME = "BdPU";

    public static void initializeEMF() {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.jdbc.user", "root");
        properties.put("javax.persistence.jdbc.password", "1234512345");
        emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
    }

    public static void closeEMF() {
        emf.close();
    }

    public static EntityManager createEntityManager() {
        if (emf == null) {
            throw new IllegalStateException("Context is not initialized yet.");
        }

        return emf.createEntityManager();
    }

    public static void closeEntityManager(EntityManager em) {
        em.close();
    }
}
