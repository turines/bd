/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.orm;

/**
 *
 * @author Patrick DAPA
 */
public class ORMFactory {

    public static Album getAlbum() {
        return new Album();
    }

    public static Album getAlbum(String pTitle, String pSerie) {
        return new Album(pTitle, pSerie);
    }

    public static Sample getSample() {
        return new Sample();
    }

    public static Sample getSample(int pYear, boolean pFirstEdition) {
        return new Sample(pYear, pFirstEdition);
    }
}
