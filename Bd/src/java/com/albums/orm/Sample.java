/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.orm;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Patrick DAPA
 */
@Entity(name = "sample")
@Table(name = "sample")
public class Sample implements Serializable {

    @Id
    @Column(name = "id_sample")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "year")
    private int year;

    @Column(name = "first_edition")
    private boolean firstEdition;

    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "album_id")
    private Album album;

    public Sample() {

    }

    public Sample(int pYear, boolean pFirstEdition) {
        this.year = pYear;
        this.firstEdition = pFirstEdition;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public boolean isFirstEdition() {
        return firstEdition;
    }

    public void setFirstEdition(boolean firstEdition) {
        this.firstEdition = firstEdition;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
