/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.dao;

/**
 *
 * @author Patrick DAPA
 */
public class DAOFactory {

    public static SampleDAO getSampleDAO() {
        return new SampleDAO();
    }

    public static AlbumDAO getAlbumDAO() {
        return new AlbumDAO();
    }
}
