/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.dao;

import com.albums.orm.Album;
import com.albums.orm.Sample;
import com.albums.service.EMF;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Patrick DAPA
 */
public class AlbumDAO implements DAO<Album> {

    /**
     * Private Entity Manager.
     */
    private final EntityManager em;

    /**
     * Constructor of an<br>
     * album manager.
     */
    public AlbumDAO() {
        EMF.initializeEMF();
        try {

        } catch (Exception e) {
        }
        em = EMF.createEntityManager();
    }

    @Override
    public List<Album> findAll() {
        TypedQuery<Album> query = em.createQuery("select a from album a", Album.class);
        return query.getResultList();
    }

    @Override
    public void delete(long id) {
        try {
            SampleDAO sampleDAO = DAOFactory.getSampleDAO();

            Album album = em.find(Album.class, id);
            em.getTransaction().begin();
            em.remove(album);
            List<Sample> samples = sampleDAO.findByAlbumId(id);
            for (Sample sample : samples) {
                sampleDAO.delete(sample.getId());
            }
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            em.getTransaction().rollback();
        }
    }

    @Override
    public void update(Album album) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.merge(album);
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            em.getTransaction().rollback();
        }
    }

    @Override
    public Album create(Album entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(entity);
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        }
        return entity;
    }

    @Override
    public void finalize() {
        try {
            EMF.closeEntityManager(em);
            super.finalize();
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Album findById(long id) {
        Album album = null;
        try {
            album = em.find(Album.class, id);
            EMF.closeEntityManager(em);
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        }
        return album;
    }
}
