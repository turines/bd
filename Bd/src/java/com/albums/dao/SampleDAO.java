/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.dao;

import com.albums.orm.Sample;
import com.albums.service.EMF;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Patrick DAPA
 */
public class SampleDAO implements DAO<Sample> {

    /**
     * Private Entity Manager.
     */
    private final EntityManager em;

    /**
     * Constructor of an<br>
     * album manager.
     */
    public SampleDAO() {
        EMF.initializeEMF();
        try {

        } catch (Exception e) {
        }
        em = EMF.createEntityManager();
    }

    @Override
    public List<Sample> findAll() {
        TypedQuery<Sample> query = em.createQuery("select s from sample s", Sample.class);
        return query.getResultList();
    }

    @Override
    public void delete(long id) {
        try {
            Sample sample = em.find(Sample.class, id);
            em.getTransaction().begin();
            em.remove(sample);
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            em.getTransaction().rollback();
        }
    }

    @Override
    public void update(Sample sample) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.merge(sample);
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
            em.getTransaction().rollback();
        }
    }

    @Override
    public Sample create(Sample entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(entity);
            em.getTransaction().commit();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        }
        return entity;
    }

    @Override
    public void finalize() {
        try {
            EMF.closeEntityManager(em);
            super.finalize();
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Sample findById(long id) {
        Sample sample = null;
        try {
            sample = em.find(Sample.class, id);
            EMF.closeEntityManager(em);
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        }
        return sample;
    }

    public List<Sample> findByAlbumId(long id) {
        Query query = em.createNativeQuery("SELECT * FROM `sample` s INNER JOIN album AS a ON a.id_album = s.album_id WHERE s.album_id = ?", Sample.class);
        query.setParameter(1, id);
        return query.getResultList();
    }

}
