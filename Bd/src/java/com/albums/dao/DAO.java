/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.dao;

import java.util.List;

/**
 *
 * @author Patrick DAPA
 * @param <T> : generic class.
 */
public interface DAO<T> {

    /**
     * Search an entity<br>
     * in the database<br>
     * according to its id.
     *
     * @return the entity.
     */
    public T findById(long id);

    /**
     * List all the entities<br>
     * in the database.
     *
     * @return the list of entities.
     */
    public List<T> findAll();

    /**
     * Deletes one entity in the database.
     *
     * @param id
     */
    public void delete(long id);

    /**
     * Updates one entity from its id.
     *
     * @param entity : entity to udpdate.
     */
    public void update(T entity);

    /**
     * Insert in the database
     *
     * @param entity : entity<br>
     * matching the table which<br>
     * we must insert into.
     */
    public T create(T entity);
}
