/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.controller;

import com.albums.model.AlbumManager;
import com.albums.model.ManagerFactory;
import com.albums.orm.Album;
import com.albums.orm.ORMFactory;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Patrick DAPA
 */
public class AlbumServlet extends HttpServlet {

    private static final String VUE = "/WEB-INF/albums.jsp";

    private static final String TITLE_PARAMETER = "title";

    private static final String SERIE_PARAMETER = "serie";

    private static final String ID_PARAMETER = "id";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Album album = null;
        AlbumManager albumManager = ManagerFactory.getAlbumManager();
        String title = null;
        String serie = null;
        long id = 0;

        String action = request.getParameter("action");
        System.out.println("action = " + action);
        if (null != action) {
            switch (action) {
                case "edit":
                    id = Long.parseLong(request.getParameter(ID_PARAMETER));
                    title = request.getParameter(TITLE_PARAMETER);
                    serie = request.getParameter(SERIE_PARAMETER);
                    album = ORMFactory.getAlbum(title, serie);
                    album.setId(id);
                    albumManager.updateAlbum(album);
                    break;
                case "delete":
                    id = Long.parseLong(request.getParameter(ID_PARAMETER));
                    System.out.println("id = " + id);
                    albumManager.RemoveAlbumFromCollection(id);
                    break;
                case "create":
                    title = request.getParameter(TITLE_PARAMETER);
                    serie = request.getParameter(SERIE_PARAMETER);
                    albumManager.addAlbumToCollection(title, serie);
                    break;
                default:
                    break;
            }
        }
        List<Album> albums = albumManager.listAllAlbums();
        request.setAttribute("albums", albums);
        this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
    }
}
