/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albums.controller;

import com.albums.model.ManagerFactory;
import com.albums.model.SampleManager;
import com.albums.orm.ORMFactory;
import com.albums.orm.Sample;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Patrick DAPA
 */
public class SampleServlet extends HttpServlet {

    private static final String VUE = "/WEB-INF/samples.jsp";

    private static final String YEAR_PARAMETER = "year";

    private static final String FIRST_EDITION_PARAMETER = "firstEdition";

    private static final String SAMPLE_ID_PARAMETER = "idSample";

    private static final String ALBUM_ID_PARAMETER = "idAlbum";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Sample sample = null;
        SampleManager sampleManager = ManagerFactory.getSampleManager();
        List<Sample> samples = null;
        int year = 0;
        boolean isFirstEdition = false;
        long idSample = 0;
        long idAlbum = 0;
        if (null != request.getParameter(ALBUM_ID_PARAMETER)) {
            idAlbum = Long.parseLong(request.getParameter(ALBUM_ID_PARAMETER));
        }
        String action = null;
        System.out.println("id album = " + request.getParameter(ALBUM_ID_PARAMETER));
        action = request.getParameter("action");
        if (null == action) {
            action = "";
        }
        System.out.println("action = " + action);
        switch (action) {
            case "edit":
                idSample = Long.parseLong(request.getParameter(SAMPLE_ID_PARAMETER));
                year = Integer.parseInt(request.getParameter(YEAR_PARAMETER));
                isFirstEdition = Boolean.valueOf(request.getParameter(FIRST_EDITION_PARAMETER));
                sample = ORMFactory.getSample(year, isFirstEdition);
                sample.setId(idSample);
                sampleManager.updateSample(sample);
                break;
            case "delete":
                idSample = Long.parseLong(request.getParameter(SAMPLE_ID_PARAMETER));
                System.out.println("id = " + idSample);
                sampleManager.RemoveSampleFromAlbum(idSample);
                break;
            case "create":
                year = Integer.parseInt(request.getParameter(YEAR_PARAMETER));
                isFirstEdition = Boolean.valueOf(request.getParameter(FIRST_EDITION_PARAMETER));
                if (0 != idAlbum) {
                    sampleManager.addSampleToAlbum(year, isFirstEdition, idAlbum);
                } else {
                    sampleManager.addSampleToCollection(year, isFirstEdition);
                }
                break;
            case "consult":
                break;
            default:
                break;
        }
        if (0 != idAlbum) {
            samples = sampleManager.listSamplesByAlbum(idAlbum);
        } else {
            samples = sampleManager.listAllSamples();
        }
        request.setAttribute("samples", samples);
        this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
    }
}
